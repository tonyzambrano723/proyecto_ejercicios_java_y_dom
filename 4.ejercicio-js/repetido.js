"use strict";
const names = [
    "A-Jay",
    "Manuel",
    "Manuel",
    "Eddie",
    "A-Jay",
    "Su",
    "Reean",
    "Manuel",
    "A-Jay",
    "Zacharie",
    "Zacharie",
    "Tyra",
    "Rishi",
    "Arun",
    "Kenton",
];
const nuevos = getUniqueListBy(names);

function getUniqueListBy(names) {
    return [...new Map(names.map((item) => [item, item])).values()];
}

console.log(nuevos);