"use strict";

function reloj() {
    var hoy = new Date();
    var h = hoy.getHours();
    var m = hoy.getMinutes();
    var s = hoy.getSeconds();

    m = actualizarHora(m);
    s = actualizarHora(s);

    document.getElementById("displayReloj").innerHTML = h + ":" + m + ":" + s;

    var t = setTimeout(function() {
        reloj();
    }, 500);
}