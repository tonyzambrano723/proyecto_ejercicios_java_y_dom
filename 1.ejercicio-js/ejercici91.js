"use strict";

const arrayPersons = [];
const url = "https://randomuser.me/api";

async function getUsers(url, amount) {
    const personas = await (await fetch(`${url}/?results=${amount}`)).json();
    for (const persona of personas.results) {
        const user = {
            username: persona.login.username,
            nombre: persona.name.first,
            apellido: persona.name.last,
            sexo: persona.gender,
            email: persona.email,
            foto: persona.picture.large,
        };
        arrayPersons.push(user);
    }
    return arrayPersons;
}
getUsers(url, 5);
console.log(arrayPersons);